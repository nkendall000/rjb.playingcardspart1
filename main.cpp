/*
	Date: 2/5/2020
	Name Part 1: Bob Baitinger
	Project: playingCards part 1

Write a C++ program that utilizes enumerations to create playing cards.
Part 1:
	Create a program that has a structure for playing cards.
	Call the struct "Card."
	The rank and suit should be enumerations.
	Note : an Ace should be a high card with a value of 14.
	Create a PUBLIC repository and upload your project to Bitbucket.
	Notify your instructor when your project is uploaded.


Part 2 :
	You will be continuing the lab exercise using a partners project(part 1), and they will use yours.
	Fork your partners repositoryand add the following functions :
	void PrintCard(Card card)
	Prints out the rankand suit of the card.Ex : The Queen of Diamonds
	Card HighCard(Card card1, Card card2)
	Determines which of the two supplied cards has the higher rank.
	Commitand push your changes to Bitbucket.
	Submit the link to your repository in Blackboard.
*/

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum _suit
{
	SUIT_HEARTS_ENUM,
	SUIT_DIAMONDS_ENUM,
	SUIT_CLUBS_ENUM,
	SUIT_SPADES_ENUM,
};

string enum_to_string(_suit type) {
	switch (type) {
	case SUIT_CLUBS_ENUM:
		return "Clubs";
	case SUIT_SPADES_ENUM:
		return "Spades";
	case SUIT_HEARTS_ENUM:
		return "Hearts";
	case SUIT_DIAMONDS_ENUM:
		return "Diamonds";
	default:
		return "Invalid card";
	}
}

enum _rank
{
	RANK_TWO_ENUM = 2,
	RANK_THREE_ENUM,
	RANK_FOUR_ENUM,
	RANK_FIVE_ENUM,
	RANK_SIX_ENUM,
	RANK_SEVEN_ENUM,
	RANK_EIGHT_ENUM,
	RANK_NINE_ENUM,
	RANK_TEN_ENUM,
	RANK_JACK_ENUM,
	RANK_QUEEN_ENUM,
	RANK_KING_ENUM,
	RANK_ACE_ENUM,
};

string rank_to_string(_rank type)
{
	switch (type) {
	case RANK_TWO_ENUM:
		return "Two";
	case RANK_THREE_ENUM:
		return "Three";
	case RANK_FOUR_ENUM:
		return "Four";
	case RANK_FIVE_ENUM:
		return "Five";
	case RANK_SIX_ENUM:
		return "Six";
	case RANK_SEVEN_ENUM:
		return "Seven";
	case RANK_EIGHT_ENUM:
		return "Eight";
	case RANK_NINE_ENUM:
		return "Nine";
	case RANK_TEN_ENUM:
		return "Ten";
	case RANK_JACK_ENUM:
		return "Jack";
	case RANK_QUEEN_ENUM:
		return "Queen";
	case RANK_KING_ENUM:
		return "King";
	case RANK_ACE_ENUM:
		return "Ace";
	default:
		return "Invalid card";
	}
}

struct Card
{
	_suit suit;
	_rank rank;
};

void PrintCard(Card card)
{
	cout << "The " << rank_to_string(card.rank) << " of " << enum_to_string(card.suit);
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else
		return card2;
}

int main(void)
{

	Card card1;
	Card card2;

	card1.rank = RANK_ACE_ENUM;
	card1.suit = SUIT_DIAMONDS_ENUM;

	card2.rank = RANK_FOUR_ENUM;
	card2.suit = SUIT_CLUBS_ENUM;

	PrintCard(HighCard(card1,card2));

	_getch();
	return 0;
}